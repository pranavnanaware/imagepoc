const admin = require("firebase-admin");
const functions = require("firebase-functions");
const express = require("express");
const bodyParser = require("body-parser");
const os = require("os");
const path = require("path");
const imageThumbnail = require("image-thumbnail");
const Busboy = require("busboy");
const key = require("../config.json");
const fs = require("fs");
const bucketName = "imagepoc-5e295.appspot.com";
admin.initializeApp();
const bucket = admin.storage().bucket(bucketName);

const runtimeOpts = {
  timeoutSeconds: 300,
  memory: "1GB",
};

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));

app.post("/", function (req: any, res: any) {
  const busboy = new Busboy({
    headers: req.headers,
  });
  const tmpdir = os.tmpdir();
  const uploads: any = {};
  const fileWrites: any = [];

  busboy.on("file", (fieldname: any, file: any, filename: any) => {
    console.log(`Processed file ${filename}`);
    const filepath = path.join(tmpdir, filename);
    console.log("PATH: " + filepath);
    uploads[fieldname] = filepath;
    const writeStream = fs.createWriteStream(filepath);
    file.pipe(writeStream);

    const promise = new Promise((resolve, reject) => {
      file.on("end", () => {
        writeStream.end();
      });
      writeStream.on("finish", resolve);
      writeStream.on("error", reject);
    });
    fileWrites.push(promise);
  });

  busboy.on("finish", () => {
    Promise.all(fileWrites)
      .then(async () => {
        for (const name in uploads) {
          const file = uploads[name];
          const data = fs.readFileSync(uploads[name]);
          const thumbnail = await imageThumbnail(data);
          const fileToUpload = bucket.file(name);
          const thumbnailToUpload = bucket.file(`thumbnail-${name}`);
          await fileToUpload.save(data, {
            resumable: false,
            public: true,
            metadata: {
              cacheControl: "public, max-age=300",
            },
          });
          await thumbnailToUpload.save(thumbnail, {
            resumable: false,
            public: true,
            metadata: {
              cacheControl: "public, max-age=300",
            },
          });
          res.send({
            imageLink: `https://storage.googleapis.com/${bucketName}/${name}`,
            thumbnailLink: `https://storage.googleapis.com/${bucketName}/thumbnail-${name}`,
          });
          fs.unlinkSync(file);
        }
      })
      .catch((err) => {
        res.json({
          uploaded: false,
          error: err,
        });
      });
  });
  busboy.end(req.rawBody);
});

exports.imageUpload = functions.runWith(runtimeOpts).https.onRequest(app);
